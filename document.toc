\contentsline {part}{I\hspace {1em}Internship background and company's organization}{6}{part.1}
\contentsline {section}{\numberline {1}Dassault Syst\IeC {\`e}mes}{7}{section.1}
\contentsline {section}{\numberline {2}Company's organization}{7}{section.2}
\contentsline {section}{\numberline {3}Goals and Specifications}{8}{section.3}
\contentsline {part}{II\hspace {1em}Presentation and analysis of the internship}{9}{part.2}
\contentsline {section}{\numberline {1}Introduction}{10}{section.1}
\contentsline {section}{\numberline {2}Related work on 3D Object classification}{10}{section.2}
\contentsline {subsection}{\numberline {2.1}Point-clouds}{11}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Multi-view images}{11}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Volumetric representations}{12}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Handcrafted signatures}{12}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Performances on a benchmark dataset}{12}{subsection.2.5}
\contentsline {section}{\numberline {3}Traceparts dataset}{13}{section.3}
\contentsline {subsection}{\numberline {3.1}Overview}{13}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Issues in the dataset}{14}{subsection.3.2}
\contentsline {section}{\numberline {4}Data preprocessing}{16}{section.4}
\contentsline {subsection}{\numberline {4.1}Normalization}{16}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Handling the imbalanced dataset}{18}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Sampling methods}{19}{subsubsection.4.2.1}
\contentsline {subsection}{\numberline {4.3}Handling mislabeled data}{20}{subsection.4.3}
\contentsline {section}{\numberline {5}Classification methods}{20}{section.5}
\contentsline {subsection}{\numberline {5.1}Evaluation measures}{20}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}k Nearest Neighbors}{20}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Random Forests and gradient boosting trees}{21}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Neural Network Classifier}{21}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Metric learning with deep learning}{21}{subsection.5.5}
\contentsline {subsection}{\numberline {5.6}Convolutional Neural Network}{22}{subsection.5.6}
\contentsline {subsection}{\numberline {5.7}Overview}{22}{subsection.5.7}
\contentsline {section}{\numberline {6}Results and analysis}{23}{section.6}
\contentsline {subsection}{\numberline {6.1}Classification results on full dataset (TP98)}{23}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Classification results with TP45 and TP20}{27}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Object recognition through a mobile application}{30}{subsection.6.3}
\contentsline {section}{\numberline {7}Conclusion and Perspectives}{30}{section.7}
\contentsline {section}{Appendices}{31}{section*.27}
\contentsline {section}{\numberline {A}RotationNet}{31}{Appendix.1.A}
\contentsline {part}{III\hspace {1em}The internship in the process of construction of the professional project}{34}{part.3}
\contentsline {section}{\numberline {1}First contact with the R\&D sector}{35}{section.1}
\contentsline {section}{References}{35}{section.1}
